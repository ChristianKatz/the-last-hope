import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class EnemyBoss extends GameObject
{
    // the handler is here to add a new object
    private Handler handler;

    // for the spawn speed of the bullet
    Random r = new Random();

    // timer fo the different movement of the enemy
    private float timer = 50;
    private float timer2 = 50;

    // the constructor, which initialize the handler,ID, y and x position of the enemy
    public EnemyBoss(int x, int y, ID id, Handler handler)
    {
        super(x, y, id);
        this.handler = handler;
        velX = 0;
        velY = 2;

    }

    // this method gets updated all the time, while the game is running
    @Override
    public void tick()
    {
        // the enemy movement
        x += velX;
        y += velY;

        // if the first timer is 0, the second timer wil start and the boss won't move on the y-axis anymore
        if(timer <= 0)
        {
            velY = 0;
            timer2--;
        }

        // if the first timer is over 0, the timer will count down
        if(timer > 0)
        {
            timer--;
        }

        // if the second timer is 0, the boss will move on the x axis all the time

        // the boss shoots bullets
        if(timer2 <= 0 )
        {
            if(velX == 0)
            {
               // velX += 0.1f * Math.signum(velX);
                velX = 5;
            }
            int spawn = r.nextInt(10);
            if(spawn == 0)
            {
                handler.addObject(new EnemyBossBullet((int) x + 48, (int) y + 48, ID.Enemy, handler));
            }
        }

        /*
        if (y <= 0 || y >= Game.HEIGHT - 32)
        {
            velY *= -1;
        }
        */

        // if the enemy has reached the end of the screen at the x -axis, the enemy will force to another direction
        if (x <= 0 || x >= Game.WIDTH - 16)
        {
            velX *= -1;
        }

        // the class "Trail" will add to the enemy
        handler.addObject(new Trail((int)x,(int)y, ID.Trail,Color.red,64,64, 0.1f, handler));
    }

    // the renderer displays the boss
    @Override
    public void render(Graphics g)
    {
        g.setColor(Color.red);
        g.fillRect((int)x,(int)y,16,16);
    }

    // the collision bounds of the enemy
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x,(int)y,64,64);
    }
}

