
import javax.swing.plaf.nimbus.State;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.security.Key;
import java.util.Random;;

public class Game extends  Canvas implements Runnable
{
    // here is the ID

    // width and Height of the game window
    public static final int WIDTH = 640, HEIGHT = WIDTH/ 12 * 9;

    // the thread of the game
    private Thread thread;

    // if the game is running or not
    private boolean running = false;

    // if the game is paused or not
    public static boolean paused = false;

    // the difficulty
    // 0 = normal
    // 1 = hard
    public int diff = 0;

    // for the random color of the menu particle
    private Random r;

    // to start the handler tick, renderer
    // to add objects
    private Handler handler;

    // to start the hud renderer and tick
    private HUD hud;

    // to start the spawner tick
    private Spawn spawner;

    // to start the menu renderer and tick
    // Mouse Listener
    private Menu menu;

    // to start the shop renderer
    // Mouse Listener
    private Shop shop;

    //different States of the game
    public enum STATE
    {
        Menu,
        Select,
        Help,
        Shop,
        Game,
        End;
    };

    // the start state is Menu
    public static STATE gameState = STATE.Menu;

    public Game()
    {
        // initializations
        handler = new Handler();
        r = new Random();
        hud = new HUD();
        shop = new Shop(handler, hud);
        menu = new Menu(this, handler, hud);
        spawner = new Spawn(handler, hud, this);

        // add listender to use the keyboard and the mouse
        this.addKeyListener(new KeyInput(handler, this));
        this.addMouseListener(menu);
        this.addMouseListener((shop));

        // creation of the window
        new Window(WIDTH, HEIGHT, "The Last Hope", this);

        // when the game starts, the player will get added to the game
       if(gameState == STATE.Game)
       {
           handler.addObject(new Player(100,100, ID.Player, handler));
       }

       // if the game State is not "Game" the particles will appear
       else
       {
           for(int i = 0; i < 10; i++)
           {
               handler.addObject(new MenuParticle(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.MenuParticle, handler));
           }
       }
    }

    // at the start of the game the thread starts and running get true
    public synchronized void start()
    {
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    // get sure that the thread terminated
    // stop the game
    // running will set to false
    public synchronized void stop() {
        try {
            thread.join();
            running = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // is responsible that frames are displayed and something playable is possible
    @Override
    public void run()
    {
        this.requestFocus();
       long lastTime = System.nanoTime();
       double amountOfTicks = 60.0;
       double ns = 1000000000 / amountOfTicks;
       double delta = 0;
       long timer = System.currentTimeMillis();
       int frames = 0;
       while(running)
       {
           long now = System.nanoTime();
           delta += (now-lastTime) / ns;
           lastTime = now;
           while(delta >= 1)
           {
               tick();
               delta--;
           }
           if(running)
           {
               render();
           }
           frames++;

           if(System.currentTimeMillis() - timer > 1000)
           {
               timer += 1000;
              // System.out.println(("FPS: " + frames));
               frames = 0;
           }
       }
       stop();
    }

    // this method gets updated all the time, while the game is running
    // update the game States permanently
    private void tick()
    {
       if(gameState == STATE.Game)
       {
           // if the game is not paused , the hud, the spawner and handler are running
           // if the game is paused, the game will freeze
           if(!paused)
           {
               hud.tick();
               spawner.tick();
               handler.tick();

               // if the health reaches 0, the end screen will appear
               // enemies get cleared
               // health is 100 again
               // menu particle will start
               if(HUD.health <= 0)
               {
                   HUD.health = 100;
                   gameState = STATE.End;
                   handler.clearEnemys();
                   for(int i = 0; i < 10; i++)
                   {
                       handler.addObject(new MenuParticle(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.MenuParticle, handler));
                   }
               }
           }
       }

       // if one of the menus are needed, the handler and the menu tick will start
       else if(gameState == STATE.Menu || gameState == STATE.End || gameState == STATE.Select)
       {
           menu.tick();
           handler.tick();
       }

    }

    // the renderer to activated the different renderer in the different game States
    // renders the window of the game
    private void render()
    {
        BufferStrategy bs = this.getBufferStrategy();

        if(bs == null)
        {
            this.createBufferStrategy(3);
            return;
        }

        Graphics g = bs.getDrawGraphics();

        g.setColor(Color.black);
        g.fillRect(0,0,WIDTH,HEIGHT);



        if(paused == true)
        {
            g.drawString("PAUSED",100,100);
        }

        if(gameState == STATE.Game)
        {
            hud.render(g);
            handler.render(g);
        }
        else if(gameState == STATE.Shop)
        {
            shop.render(g);
        }

        else if(gameState == STATE.Menu || gameState == STATE.End || gameState == STATE.Help ||gameState == STATE.Select)
        {
            menu.render(g);
            handler.render(g);
        }

        g.dispose();
        bs.show();
    }

    public static void main(String args[])
    {
       new Game();
    }
}
