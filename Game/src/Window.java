import javax.swing.*;
import java.awt.*;
import java.awt.Dimension;

//In der objektorientierten Programmierung  gehört also jedes Objekt zu einer Klasse. Dieses besitzt die Attribute (Eigenschaften) und die Methoden (Interaktionen) dieser Klasse.
// Klasse : Mensch
// Object/Subclass: einzelne Person
// Attribut: Augenfarbe

public class Window extends Canvas
{
    // in the video is here a ID

    // the constructor, which initialize the width, height, title and game
    // the frame of the game will get created here
    public Window(int width, int height, String title, Game game)
    {
        JFrame frame = new JFrame(title);

        frame.setPreferredSize(new Dimension(width, height));
        frame.setMaximumSize(new Dimension(width, height));
        frame.setMinimumSize(new Dimension(width, height));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.add(game);
        frame.setVisible(true);
        game.start();
    }

}
