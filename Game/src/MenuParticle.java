import java.awt.*;
import java.util.Random;

public class MenuParticle extends GameObject
{
    // handler to add objects
    private Handler handler;

    // random numbers for the particle color and for the decision between two different speeds
    Random r = new Random();

    // color of the particle
    private Color col;

    // decision between different speeds
    int dir = 0;

    // the constructor, which initialize the handler,ID, y and x position of the particle
    public MenuParticle(int x, int y, ID id, Handler handler)
    {
        super(x, y, id);
        this.handler = handler;

        // random number between 0 and 1
        dir = r.nextInt(1);

        // two different speeds
        if(dir == 0)
        {
            velX = 2;
            velY = 5;
        }
        else if(dir == 1)
        {
            velX = 5;
            velY = 2;
        }

        // color of the particle
        col = new Color (r.nextInt(255), r.nextInt(255), r.nextInt(255));
    }

    // this method gets updated all the time, while the game is running
    @Override
    public void tick()
    {
        // the enemy movement
        x += velX;
        y += velY;

        // if the particle has reached the end of the screen at the y-axis, the particle will get forced to another direction
        if (y <= 0 || y >= Game.HEIGHT - 32)
        {
            velY *= -1;
        }

        // if the particle has reached the end of the screen at the x-axis, the particle will get forced to another direction
        if (x <= 0 || x >= Game.WIDTH - 16)
        {
            velX *= -1;
        }

        // the class "Trail" will add to the particle
        handler.addObject(new Trail((int)x,(int)y, ID.Trail,col,16,16, 0.05f, handler));
    }

    // the renderer displays the particles
    @Override
    public void render(Graphics g)
    {
        g.setColor(col);
        g.fillRect((int)x,(int)y,16,16);
    }

    // the collision bounds of the particle
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x,(int)y,16,16);
    }
}

