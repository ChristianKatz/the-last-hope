import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

// Key Input of the whole game
public class KeyInput extends KeyAdapter
{
    // the handler to get the objects and player speed
    private Handler handler;

    // shows, which key is down or not to have a smoother movement
    private boolean[] keyDown = new boolean[4];

    // declaration of the main class "game" to get the different game states
    Game game;

    // constructor that initialize the handler and the game classes
    // at the start of the game the player doesn't move yet so that the keyDown boolean is set to false
    public KeyInput(Handler handler, Game game)
    {
        this.handler = handler;
        this.game = game;

        keyDown[0] = false;
        keyDown[1] = false;
        keyDown[2] = false;
        keyDown[3] = false;
    }



    public void keyPressed(KeyEvent e)
    {
        // get the Key codes so that the pc knows which button is pressed
        // then the handler is asked after an object called "player"
        // when the ID is the player, the keys can be pressed to set the velocity
        // when a key is pressed, the player moves in a certain direction
        int key = e.getKeyCode();

        for(int i = 0; handler.object.size() > i; i++)
        {
            GameObject tempObject = handler.object.get(i);

            if(tempObject.getId() == ID.Player)
            {
                if(key == KeyEvent.VK_W)
                {
                    tempObject.setVelY(-handler.speed);
                    keyDown[0] = true;
                }
                if(key == KeyEvent.VK_S)
                {
                    tempObject.setVelY(+handler.speed);
                    keyDown[1] = true;
                }
                if(key == KeyEvent.VK_A)
                {
                    tempObject.setVelX(-handler.speed);
                    keyDown[2] = true;
                }
                if(key == KeyEvent.VK_D)
                {
                    tempObject.setVelX(+handler.speed);
                    keyDown[3] = true;
                }
            }
        }

        // when the P button is pressed, the game will be paused
        if(key== KeyEvent.VK_P)
        {
            if(game.gameState == Game.STATE.Game)
            {
                if(Game.paused == true)
                {
                    Game.paused = false;
                }
                else
                {
                    Game.paused = true;
                }
            }
        }

        // if the player presses the ESC button, the game will get closed
        if(key == KeyEvent.VK_ESCAPE)
        {
            System.exit(1);
        }

        // if the player presses the Space bar, the shop will get opened and the game stop
        if(key == KeyEvent.VK_SPACE)
        {
            if(Game.gameState == Game.STATE.Game)
            {
                Game.gameState = Game.STATE.Shop;
            }
            else if(game.gameState == Game.STATE.Shop)
            {
                Game.gameState = Game.STATE.Game;
            }
        }
    }
    public void keyReleased(KeyEvent e)
    {
        // get the Key codes so that the pc knows which button is released
        // then the handler is asked after an object called "player"
        // when the ID is the player, the bool value get to false
        // then the velocity will get set to 0 on the certain axis
        int key = e.getKeyCode();

        for(int i = 0; handler.object.size() > i; i++)
        {
            GameObject tempObject = handler.object.get(i);

            if(tempObject.getId() == ID.Player)
            {
                if(key == KeyEvent.VK_W)
                {
                    keyDown[0] = false;
                }
                if(key == KeyEvent.VK_S)
                {
                    keyDown[1] = false;
                }
                if(key == KeyEvent.VK_A)
                {
                    keyDown[2] = false;
                }
                if(key == KeyEvent.VK_D )
                {
                    keyDown[3] = false;
                }

                if( !keyDown[0] && !keyDown[1])
                {
                    tempObject.setVelY(0);
                }
                if( !keyDown[2] && !keyDown[3])
                {
                    tempObject.setVelX(0);
                }

            }
        }

    }
}
