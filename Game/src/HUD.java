import java.awt.*;

public class HUD
{
    // the health bound
    public int bounds = 0;

    // the health of the player
    public static float health = 100;

    // the green color value
    private float greenValue = 255;

    // the score in the game
    private int score = 0;

    // the levels of the game
    private int level = 1;

    // this method gets updated all the time, while the game is running
    public void tick()
    {
        // if the health and the green value reaches 0, the values should keep at 0
         if(health <= 0)
         {
             health = 0;
             greenValue = 0;
         }

         // at the beginning of the game the health is 100 and will get multiplies by 2
         // because the green value should be higher to have a brighter green
         greenValue = health * 2;

         // if updates are bought, it can happen that the green value gets above 255, but that is limit of RGB
         // thus the maximal value has set to 255
        if(greenValue > 254)
        {
            greenValue = 255;
        }

         // the score gets permanently increased
         score++;
    }

    // the renderer for the HUD displays
    public void render(Graphics g)
    {
         g.setColor(Color.gray);
         g.fillRect(15,15, 200 + bounds, 32 );
         g.setColor(new Color(75,(int)greenValue,0));
         g.fillRect(15,15,(int)health * 2, 32);
         g.setColor(Color.white);
         g.drawRect(15,15, 200 + bounds,32);

         g.drawString("Score: " + score, 15,64);
         g.drawString("Level: " + level,15,80);
         g.drawString("Space for Shop", 15,94);
    }

    //variables to set and get the score and levels in other classes

    public void setScore(int score)
    {
        this.score = score;
    }
    public int getScore()
    {
        return score;
    }
    public int getLevel()
    {
        return level;
    }
    public void setLevel(int level)
    {
        this.level = level;
    }

}
