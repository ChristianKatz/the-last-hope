import java.awt.*;

// the objects get created in this class
public abstract class GameObject
{
    // only the objects which inherit the GameObject are able to set the position, id and the speed of the newly created object
    protected  float x,y;
    protected ID id;
    protected float velX, velY;

    // the constructor, which initialize the handler,ID, y and x position of the enemy
    public GameObject(float x, float y, ID id)
    {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    // this method updates the object all the time, while the game is running
    public abstract void tick();

    // the renderer for the object
    public abstract void render(Graphics g);

    // the collision bounds of the object
    public abstract Rectangle getBounds();

    // to set and get the x and y position of the object and the velocity on the x and y axis
    public void setX(int x)
    {
        this.x = x;
    }
    public void setY( int y)
    {
        this.y = y;
    }
    public void setId( ID id)
    {
        this.id = id;
    }
    public void setVelX( int velX)
    {
        this.velX = velX;
    }
    public void setVelY( int velY)
    {
        this.velY = velY;
    }
    public float getX()
    {
        return x;
    }
    public float getY()
    {
        return y;
    }
    public ID getId()
    {
        return id;
    }
    public float getVelX()
    {
        return velX;
    }
    public float getVelY()
    {
        return velY;
    }

}
