import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class EnemyBossBullet extends GameObject
{
    // the handler is to remove and add objects
    private Handler handler;

    // give the bullet a different direction
    Random r = new Random();

    // the constructor, which initialize the handler, ID, y and x position of the bullet
    public EnemyBossBullet(int x, int y, ID id, Handler handler)
    {
        super(x, y, id);
        this.handler = handler;
        velX = (r.nextInt(5- -5) + -5);
        velY = 2;
    }

    // this method gets updated all the time, while the game is running
    @Override
    public void tick()
    {
        // the boss movement
        x += velX;
        y += velY;

        // if the bullet is further than the game screen at the bottom, the bullet will get destroyed
        if(y >= Game.HEIGHT)
        {
            handler.removeObject(this);
        }

        // the class "Trail" will add to the enemy
        handler.addObject(new Trail((int)x,(int)y, ID.Trail,Color.red,16,16, 0.05f, handler));
    }

    // the renderer displays the bullets
    @Override
    public void render(Graphics g)
    {
        g.setColor(Color.red);
        g.fillRect((int)x,(int)y,16,16);
    }

    // the collision bounds of the bullets
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x,(int)y,16,16);
    }
}


