import java.awt.*;
import java.awt.image.BufferedImage;

public class Player extends GameObject
{
    // the handler is here to add a new object and to get information about objects
    Handler handler;

    // the constructor, which initialize the handler,ID, y and x position of the player
    public Player(int x, int y, ID id, Handler handler)
    {
        super(x,y,id);
        this.handler = handler;
    }

    // this method gets updated all the time, while the game is running
    @Override
    public void tick()
    {
        // the player movement
       x+= velX;
       y+= velY;

        // if the player reaches the end of the screen on the top side, then the cube isn't moving any further
       if(y <= 0)
       {
           y = 0;
       }

        // if the player reaches the end of the screen on the bottom side, then the cube isn't moving any further
       if(y >= Game.HEIGHT -72)
       {
           y = Game.HEIGHT -72;
       }

        // if the player reaches the end of the screen on the left side, then the cube isn't moving any further
       if(x <= 0)
       {
           x = 0;
       }

        // if the player reaches the end of the screen on the right side, then the cube isn't moving any further
       if(x >= Game.WIDTH -48)
       {
           x = Game.WIDTH -48;
       }

        // the class "Trail" will add to the player
        handler.addObject(new Trail((int) x,(int) y, ID.Trail,Color.white,32,32, 0.05f, handler));

       collision();

    }

    // this method is for the player collision
    // all objects get checked if the object is an enemy or not
    // if yes the player get damage at the collision with the enemy
    private void collision()
    {
        for(int i = 0; i < handler.object.size(); i++)
        {
            GameObject tempObject = handler.object.get(i);

            if(tempObject.getId() == ID.Enemy || tempObject.getId() == ID.SmartEnemy || tempObject.getId() == ID.EnemyBoss || tempObject.getId() == ID.HardEnemy)
            {
                if(getBounds().intersects(tempObject.getBounds()))
                {
                     HUD.health--;
                }
            }

        }
    }

    // the renderer displays the player
    @Override
    public void render(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.green);
        g2d.draw(getBounds());

       //g.setColor(Color.white);
       //g.fillRect(x,y,32,32);
    }

    // the collision bounds of the player
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x,(int)y,32,32);
    }
}
