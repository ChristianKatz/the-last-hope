import java.awt.*;
import java.util.LinkedList;

// update and render all the objects in the game(Enemy,Trail,Hud,Spawn,Player,etc)
public class Handler
{
    // the list that takes all added objects
   LinkedList<GameObject> object = new LinkedList<GameObject>();

   // the player movement speed
   public int speed = 5;

    // this method gets updated all the time, while the game is running
   public void tick()
   {
       for(int i = 0; i < object.size(); i++)
       {
          GameObject tempObject = object.get(i);

          tempObject.tick();
       }
   }

    // the renderer for the objects
   public void render(Graphics g)
   {
       for(int i = 0; i < object.size(); i++)
       {
           GameObject tempObject = object.get(i);

           tempObject.render(g);
       }
   }

   // clear all enemies on the screen
    public void clearEnemys()
    {
        for(int i = 0; i < object.size(); i++)
        {
            GameObject tempObject = object.get(i);

           if(tempObject.getId() != ID.Player)
           {
               removeObject(tempObject);
               i--;
           }
           //clear all objects on the screen, when the player finished the game or dies
           if(Game.gameState == Game.STATE.End)
           {
              object.clear();
           }
        }
    }

    // add objects to the handler
   public void addObject(GameObject object)
   {
       this.object.add(object);
   }

   // remove objects to the handler
   public void removeObject(GameObject object)
   {
       this.object.remove(object);
   }
}

