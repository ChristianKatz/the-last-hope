import java.awt.*;

public class SmartEnemy extends GameObject
{
    // the handler add the Trail
    private Handler handler;

    //  the GameObject is for position of the player
    private GameObject player;

    // the constructor, which initialize the handler, x and y position and the id of the enemy
    public SmartEnemy(int x, int y, ID id, Handler handler)
    {
        super(x, y, id);
        this.handler = handler;

        // get the object of the player
        for(int i = 0; i < handler.object.size(); i++)
        {
            GameObject tempobject = handler.object.get(i);

            if(tempobject.getId() == ID.Player)
            {
              player = handler.object.get(i);
            }
        }

        // enemy speed
        velX = 5;
        velY = 5;

    }

    // this method gets updated all the time, while the game is running
    @Override
    public void tick()
    {
        // the movement of the player
        x += velX;
        y += velY;

        // add the trail to the enemy
        handler.addObject(new Trail((int)x,(int)y, ID.Trail,Color.green,16,16, 0.05f, handler));

        // get the distance between the player and the smart enemy
        float diffX = x - player.getX();
        float diffY = y - player.getY();
        float distance = (float)Math.sqrt((x-player.getX())*(x-player.getX()) + (y-player.getY())*(y-player.getY()));

        // speed of the enemy
        velX = ((-1/distance) * diffX);
        velY = ((-1/distance) * diffY);
    }

    // the renderer displays the SmartEnemy
    @Override
    public void render(Graphics g)
    {
        g.setColor(Color.red);
        g.fillRect((int)x,(int)y,16,16);
    }

    // the collision bounds of the smart enemy
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x,(int)y,16,16);
    }
}
