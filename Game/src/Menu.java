import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class Menu extends MouseAdapter
{
    // the game is for the game states
    private Game game;

    // the handler is for the object clear and add responsible
    private Handler handler;

    // the hud set the level and score
    private HUD hud;

    // constructor that initialize the handler, game and hud
    public Menu(Game game, Handler handler, HUD hud)
    {
        this.game = game;
        this.handler = handler;
        this.hud = hud;
    }


    public void mousePressed(MouseEvent e)
    {
        // the mouse position
        int mx = e.getX();
        int my = e.getY();

        //when the player is in the menu, then he is able to click on the buttons
        if (game.gameState == Game.STATE.Menu)
        {
            // the player gets in the select meu, where he can choose between different difficulties
            if (mouseOver(mx, my, 210, 150, 200, 64))
            {
                game.gameState = Game.STATE.Select;
                return;
            }

            // the player gets in the help window, where the game is explained
            if (mouseOver(mx, my, 210, 250, 200, 64))
            {
                game.gameState = Game.STATE.Help;
            }

            // the player exits the game
            if (mouseOver(mx, my, 210, 350, 200, 64))
            {
                System.exit(1);
            }

        }

        // if the player is in the selection, he can choose between normal and hard
        // when the player selected a level, the game will get cleared of enemies, game state "Game" is selected and the corresponding enemies are spawning
        // the int diff will get a 0 or 1 so that the corresponding enemies can spawn in the other class
        if (game.gameState == Game.STATE.Select)
        {
            //normal
            if (mouseOver(mx, my, 210, 150, 200, 64))
            {
                game.gameState = Game.STATE.Game;
                handler.clearEnemys();
                handler.addObject(new Player(100,100, ID.Player, handler));
                for(int i = 0; i <= 1; i++)
                {
                    handler.addObject(new HardEnemy(Game.WIDTH / 2, 0, ID.HardEnemy, handler));
                }
                game.diff = 0;
            }

            //hard
            if (mouseOver(mx, my, 210, 250, 200, 64))
            {
                game.gameState = Game.STATE.Game;
                handler.clearEnemys();
                handler.addObject(new Player(100,100, ID.Player, handler));
                for(int i = 0; i <= 0; i++)
                {
                    handler.addObject(new EnemyBoss(Game.WIDTH / 2, -60, ID.EnemyBoss, handler));
                }

                game.diff = 1;
            }

            // back button in the select menu
            if (mouseOver(mx, my, 210, 350, 200, 64))
            {
                game.gameState = Game.STATE.Menu;
            }

        }


            //back button in the help window
        if (game.gameState == Game.STATE.Help)
        {
            if (mouseOver(mx, my, 210, 350, 200, 64))
            {
                game.gameState = Game.STATE.Menu;
            }
        }

        // if the enemy dies, the the score will be set back to 0 and the level to 1 again
        // teh game state is menu
        if (game.gameState == Game.STATE.End)
        {
            if (mouseOver(mx, my, 210, 350, 200, 64))
            {
                game.gameState = Game.STATE.Menu;
                hud.setScore(0);
                hud.setLevel(1);
            }
        }

    }

    public void mouseReleased(MouseEvent e) {

    }

    // method to check if the mouse is in the determined area
    private boolean mouseOver(int mx, int my, int x, int y, int width, int height) {
        if (mx > x && mx < x + width) {
            if (my > y && my < y + height) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public void tick() {

    }

    // the renderer displays the different menus for the corresponding game states
    public void render(Graphics g) {
        if (game.gameState == Game.STATE.Menu) {
            Font font = new Font("arial", 1, 50);
            Font font2 = new Font("arial", 1, 30);

            g.setFont(font);
            g.setColor(Color.white);
            g.drawString("Menu", 245, 50);

            g.setFont(font2);
            g.drawRect(210, 150, 200, 64);
            g.drawString("Play", 280, 190);


            g.drawRect(210, 250, 200, 64);
            g.drawString("Help", 280, 290);


            g.drawRect(210, 350, 200, 64);
            g.drawString("Quit", 280, 390);
        } else if (game.gameState == Game.STATE.Help) {
            Font font = new Font("arial", 1, 50);
            Font font2 = new Font("arial", 1, 30);

            g.setFont(font);
            g.setColor(Color.white);
            g.drawString("Help", 245, 50);

            g.setFont(font2);
            g.drawRect(210, 350, 200, 64);
            g.drawString("Back", 280, 390);
        } else if (game.gameState == Game.STATE.End) {
            Font font = new Font("arial", 1, 50);
            Font font2 = new Font("arial", 1, 30);
            Font font3 = new Font("arial", 1, 20);

            g.setFont(font);
            g.setColor(Color.white);
            g.drawString("Game Over", 180, 50);

            g.setFont(font3);
            g.setColor(Color.white);
            g.drawString("You lost with a score of: " + hud.getScore(), 175, 200);

            g.setFont(font2);
            g.drawRect(210, 350, 200, 64);
            g.drawString("Menu", 270, 390);
        }
        if (game.gameState == Game.STATE.Select) {
            Font font = new Font("arial", 1, 50);
            Font font2 = new Font("arial", 1, 30);

            g.setFont(font);
            g.setColor(Color.white);
            g.drawString("Select Difficulty", 245, 50);

            g.setFont(font2);
            g.drawRect(210, 150, 200, 64);
            g.drawString("Normal", 280, 190);


            g.drawRect(210, 250, 200, 64);
            g.drawString("Hard", 280, 290);


            g.drawRect(210, 350, 200, 64);
            g.drawString("Back", 280, 390);

        }
    }
}
