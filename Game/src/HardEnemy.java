import java.awt.*;

public class HardEnemy extends GameObject
{
    // the handler is here to add a new object
    private Handler handler;

    // the constructor, which initialize the handler,ID, y and x position of the enemy
    public HardEnemy(int x, int y, ID id, Handler handler)
    {
        super(x, y, id);
        this.handler = handler;
        velX = 5;
        velY = 5;

    }

    // this method gets updated all the time, while the game is running
    @Override
    public void tick()
    {
        // the enemy movement
        x += velX;
        y += velY;

        // if the enemy has reached the end of the screen at the y-axis, the enemy will get forced to another direction
        if (y <= 0 || y >= Game.HEIGHT - 32)
        {
            velY *= -1;
        }

        // if the enemy has reached the end of the screen at the x-axis, the enemy will get forced to another direction
        if (x <= 0 || x >= Game.WIDTH - 16)
        {
            velX *= -1;
        }

        // the class "Trail" will add to the enemy
        handler.addObject(new Trail((int)x,(int)y, ID.Trail,Color.yellow,16,16, 0.05f, handler));
    }

    // the renderer displays the hard enemy
    @Override
    public void render(Graphics g)
    {
        g.setColor(Color.yellow);
        g.fillRect((int)x,(int)y,16,16);
    }

    // the collision bounds of the enemy
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x,(int)y,16,16);
    }
}

