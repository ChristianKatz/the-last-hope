import java.util.Random;

public class Spawn
{
    // the game class to know on with difficulty the player want to play
    private Game game;

    // the handler to add new objects
    private Handler handler;

    // the hud to get and set the level of the player
    private HUD hud;

    // pseudorandom number for the next enemy spawn position
    private Random r = new Random();

    // is for the level increase
    private int scoreKeep = 0;

    // the constructor, which initialize the handler, hud and game of the player
    public Spawn(Handler handler, HUD hud, Game game)
    {
        this.handler = handler;
        this.hud = hud;
        this.game = game;

    }

    // this method gets updated all the time, while the game is running
    public void tick()
    {
        // increase permanently the score
       scoreKeep++;

       // if the player selects the first difficulty and the score is over a certain value, the level will get increased
        // at the certain levels spawn the different enemies
       if(game.diff == 0)
       {
           if(scoreKeep >= 100)
           {
               scoreKeep = 0;
               hud.setLevel(hud.getLevel() + 1);

               if(hud.getLevel() == 1)
               {
                   handler.addObject(new EnemyBoss(r.nextInt(Game.WIDTH),r.nextInt(Game.HEIGHT), ID.Enemy, handler));
               }
               if(hud.getLevel() == 10)
               {
                   handler.addObject(new SmartEnemy(r.nextInt(Game.WIDTH),r.nextInt(Game.HEIGHT), ID.SmartEnemy, handler));
               }
           }
       }
        if(game.diff == 1)
        {
            if(scoreKeep >= 100)
            {
                scoreKeep = 0;
                hud.setLevel(hud.getLevel() + 1);

                if(hud.getLevel() == 1)
                {
                    handler.addObject(new EnemyBoss(r.nextInt(Game.WIDTH),r.nextInt(Game.HEIGHT), ID.Enemy, handler));
                }
                if(hud.getLevel() == 10)
                {
                    handler.addObject(new SmartEnemy(r.nextInt(Game.WIDTH),r.nextInt(Game.HEIGHT), ID.SmartEnemy, handler));
                }
            }
        }
    }
}
