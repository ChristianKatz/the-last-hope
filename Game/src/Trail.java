import java.awt.*;

public class Trail extends GameObject
{
    // the alpha value for the transparent
    private float alpha = 1;

    // the handler is for the removing of the Trail
    private Handler handler;

    // Color of the Trail
    private Color color;

    // height and width of the Trail
    private int width;
    private int height;

    // lifetime of the Trail
    private float life;

    // the constructor, which initialize the x and y position, id, color, width, height, lifetime and handler of the Trail
    public Trail(int x, int y, ID id,Color color, int width, int height, float life, Handler handler)
    {
        super(x, y, id);
        this.handler = handler;
        this.color = color;
        this.width = width;
        this.height = height;
        this.life = life;
    }

    // this method gets updated all the time, while the game is running
    @Override
    public void tick()
    {
        // if the alpha value is greater than the lifetime, the Trail still exist
        if(alpha > life)
        {
            alpha -= life - 0.001f;
        }

        // if the trail is not visible anymore, it will get removed
        else
        {
            handler.removeObject(this);
        }
    }

    // the renderer displays the Trail
    @Override
    public void render(Graphics g)
    {
         Graphics2D g2d = (Graphics2D) g;
         g2d.setComposite(makeTransparent(alpha));

         g.setColor(color);
         g.fillRect((int)x,(int)y,width,height);

        g2d.setComposite(makeTransparent(1));

    }

    // the method to fade the Trail
    private AlphaComposite makeTransparent(float alpha)
    {
        int type = AlphaComposite.SRC_OVER;

        return (AlphaComposite.getInstance(type, alpha));
    }

    // the Trail doesn't have any collision
    @Override
    public Rectangle getBounds()
    {
        return null;
    }
}
