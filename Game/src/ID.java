// The IDs of specific object
public enum ID
{
    Player(),
    Trail(),
    Enemy(),
    EnemyBoss(),
    MenuParticle(),
    SmartEnemy(),
    HardEnemy();
}
